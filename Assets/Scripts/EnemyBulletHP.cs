using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletHP : MonoBehaviour
{
    //HPゲージ
    [SerializeField] int enemyBulletHP;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    //被ダメージ処理
    public void ReceveDamage(int damageScore)
    {
        enemyBulletHP -= damageScore;
        Debug.Log("enemyBulletHP:" + enemyBulletHP);
        //オーバーキルでHPが0を飛び越えても対処できるよう、判定を0以下にしておく
        if (enemyBulletHP <= 0)
        {
            //このスクリプトがアタッチされているオブジェクトを消す
            Destroy(gameObject);
        }
    }
}