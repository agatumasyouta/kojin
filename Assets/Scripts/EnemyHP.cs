using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    //HPゲージ
    [SerializeField] int enemyHP;//追記部分

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    //被ダメージ処理
    public void ReceveDamage(int damageScore)
    {
        enemyHP -= damageScore;
        Debug.Log("enemyHP:" + enemyHP);
        //オーバーキルでHPが0を飛び越えても対処できるよう、判定を0以下にしておく
        if (enemyHP <= 0)
        {
            //このスクリプトがアタッチされているオブジェクトを消す
            Destroy(gameObject);//追記 他のスクリプトと表記を合わせる為に「this.」を削除
            //以下、追記部分
            //GameObject型の配列enemyBulletsに、"EnemyBullet"タグのついたオブジェクトをすべて格納
            GameObject[] enemyBullets = GameObject.FindGameObjectsWithTag("EnemyBullet");
            //foreach(配列の要素の数だけループ)を使って、
            //enemyBulletにenemyBullets配列の中身を１つずつ取り出す。
            foreach (GameObject enemyBullet in enemyBullets)
            {
                Destroy(enemyBullet);
            }
            //以上、追記部分
        }
    }
}