using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracking : MonoBehaviour
{
    GameObject Player;
    [SerializeField] float Speed;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
    }

    //衝突判定
    void OnTriggerEnter(Collider other)
    {
        //衝突したオブジェクトのタグがPlayerだった場合
        if (other.tag == "Player")
        {
            //このスクリプトがアタッチされているオブジェクト(自分自身)を消す。
            Destroy(gameObject);
        }
    }
}