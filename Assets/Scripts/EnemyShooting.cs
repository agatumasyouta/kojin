using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    [SerializeField] GameObject enemyBulletPrefab;
    [SerializeField] float shotInterval;
    float timeCount;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //時間計測
        timeCount += Time.deltaTime;
        //指定の秒数以上になった場合
        if (timeCount >= shotInterval)
        {
            //カウントをリセット
            timeCount = 0;
            //弾を生成
            Instantiate(enemyBulletPrefab, transform.position, Quaternion.identity);
        }
    }
}