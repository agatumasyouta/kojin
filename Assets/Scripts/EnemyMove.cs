using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    //ゲームオブジェクトの情報を取得する為の変数
    GameObject player, wall, wall_1, wall_2, wall_3;
    //レイが当たったオブジェクトの情報を取得する為の変数
    RaycastHit hitXpositive, hitXnegative, hitZpositive, hitZnegative;
    //Enemyの大きさ、Wallの大きさ、レイの長さ、間隔を調整する為の変数
    float enemySize, wallSize, enemyRayLength, spaceAdjustment;
    //Enemyの移動速度、半分の大きさにする為の係数
    int enemyTopSpeed, enemySpeed;
    const int HALFSIZE = 2;
    //レイの接触判定フラグ
    bool rayXpositiveTouch, rayXnegativeTouch, rayZpositiveTouch, rayZnegativeTouch;
    // Start is called before the first frame update
    void Start()
    {
        //Playerタグのオブジェクトを取得
        player = GameObject.FindGameObjectWithTag("Player");
        //Wallオブジェクトを取得
        wall = GameObject.Find("Wall");
        wall_1 = GameObject.Find("Wall (1)");
        wall_2 = GameObject.Find("Wall (2)");
        wall_3 = GameObject.Find("Wall (3)");
        //EnemyオブジェクトのMeshRendererに含まれる矩形からx辺の長さを取得
        enemySize = GetComponent<MeshRenderer>().bounds.size.x;
        //WallオブジェクトのMeshRendererに含まれる矩形からy辺の長さを取得
        wallSize = wall.GetComponent<MeshRenderer>().bounds.size.y;
        //Enemyオブジェクト１辺分の長さ
        enemyRayLength = enemySize;
        //EnemyとWallとの間隔調整
        spaceAdjustment = 1.5f;
        //Enemyの移動速度
        enemyTopSpeed = 5000;
        enemySpeed = 3000;
        
    }

    // Update is called once per frame
    void Update()
    {
        //レイがどこにも接触していなかったら（ゲーム開始時点）
        if (!rayXpositiveTouch && !rayXnegativeTouch && !rayZpositiveTouch && !rayZnegativeTouch)
        {
            //Wallの端からEnemy1.5個分(原点が基準なので、Enemy1.5個分で、壁から1個分空いているように見える)間隔を空けた位置に向けて移動。
            //MoveTowardsを使う事でフレームやスピードの差異に関わらず指定位置に止める事ができる。
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(wall_2.transform.position.x, wall_2.transform.position.y, wall_2.transform.position.z - enemySize * spaceAdjustment), Time.deltaTime * enemyTopSpeed);
        }
        //正のX方向のレイが接触していたら
        if (rayXpositiveTouch)
        {
            //Wallの原点に正のZ方向へWall半分プラスした位置 = Wallの端
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, wall_3.transform.position.z + wallSize / HALFSIZE - enemySize * spaceAdjustment), Time.deltaTime * enemySpeed);
        }
        //正のZ方向のレイが接触していたら
        if (rayZpositiveTouch)
        {
            //Wallの原点に負のX方向へWall半分マイナスした位置 = Wallの端
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(wall_2.transform.position.x - wallSize / HALFSIZE + enemySize * spaceAdjustment, transform.position.y, transform.position.z), Time.deltaTime * enemySpeed);
        }
        //負のX方向のレイが接触していたら
        if (rayXnegativeTouch)
        {
            //Wallの原点に負のZ方向へWall半分マイナスした位置 = Wallの端
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, wall_1.transform.position.z - wallSize / HALFSIZE + enemySize * spaceAdjustment), Time.deltaTime * enemySpeed);
        }
        //負のZ方向のレイが接触していたら
        if (rayZnegativeTouch)
        {
            //Wallの原点に正のX方向へWall半分マイナスした位置 = Wallの端
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(wall.transform.position.x + wallSize / HALFSIZE - enemySize * spaceAdjustment, transform.position.y, transform.position.z), Time.deltaTime * enemySpeed);
        }
        //Enemyの原点に半径分プラスした位置からX方向とZ方向にレイを飛ばす
        Ray rayXpositive = new Ray(transform.position + new Vector3(enemySize / HALFSIZE, 0, 0), Vector3.right);
        Ray rayXnegative = new Ray(transform.position + new Vector3(-enemySize / HALFSIZE, 0, 0), Vector3.left);
        Ray rayZpositive = new Ray(transform.position + new Vector3(0, 0, enemySize / HALFSIZE), Vector3.forward);
        Ray rayZnegative = new Ray(transform.position + new Vector3(0, 0, -enemySize / HALFSIZE), Vector3.back);
        //rayの可視化
        Debug.DrawRay(rayXpositive.origin, rayXpositive.direction * enemyRayLength, Color.magenta);
        Debug.DrawRay(rayXnegative.origin, rayXnegative.direction * enemyRayLength, Color.magenta);
        Debug.DrawRay(rayZpositive.origin, rayZpositive.direction * enemyRayLength, Color.cyan);
        Debug.DrawRay(rayZnegative.origin, rayZnegative.direction * enemyRayLength, Color.cyan);
        //正のX方向のレイが壁に接触していたら、フラグON
        if (Physics.Raycast(rayXpositive, out hitXpositive, enemyRayLength))
        {
            rayXpositiveTouch = true;
        }
        //正のX方向のレイが壁に接触していなかったら、フラグOFF
        else if (!Physics.Raycast(rayXpositive, out hitXpositive, enemyRayLength))
        {
            rayXpositiveTouch = false;
        }
        //負のX方向のレイが壁に接触していたら、フラグON
        if (Physics.Raycast(rayXnegative, out hitXnegative, enemyRayLength))
        {
            rayXnegativeTouch = true;
        }
        //負のX方向のレイが壁に接触していなかったら、フラグOFF
        else if (!Physics.Raycast(rayXnegative, out hitXnegative, enemyRayLength))
        {
            rayXnegativeTouch = false;
        }
        //正のZ方向のレイが壁に接触していたら、フラグON
        if (Physics.Raycast(rayZpositive, out hitZpositive, enemyRayLength))
        {
            rayZpositiveTouch = true;
        }
        //正のZ方向のレイが壁に接触していなかったら、フラグOFF
        else if (!Physics.Raycast(rayZpositive, out hitZpositive, enemyRayLength))
        {
            rayZpositiveTouch = false;
        }
        //負のZ方向のレイが壁に接触していたら、フラグON
        if (Physics.Raycast(rayZnegative, out hitZnegative, enemyRayLength))
        {
            rayZnegativeTouch = true;
        }
        //負のZ方向のレイが壁に接触していなかったら、フラグOFF
        else if (!Physics.Raycast(rayZnegative, out hitZnegative, enemyRayLength))
        {
            rayZnegativeTouch = false;
        }
        //常にPlayerの方を向く
        transform.LookAt(player.transform);
    }
}