using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour
{
    //HPゲージ
    [SerializeField] int playerHP;

    //被ダメージ量
    [SerializeField] int receveDamageScore;

    //被ダメージ演出用ボード
    public Image damageBoard, deathStateBoard;

    //HPゲージ用のUI複数をまとめたオブジェクト
    public GameObject remainingHp;

    //カメラの動きを制御する
    Transform transformCamera;

    //カメラを揺らす時間の長さ、大きさ
    [SerializeField] float duration, magnitude;

    //相対位置取得用
    Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        //被ダメージ演出用ボードを透明に初期化
        damageBoard.color = Color.clear;

        //カメラのTransformを取得
        transformCamera = Camera.main.transform;

        //Playerからの相対位置を取得
        pos = transformCamera.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        //弾にぶつかったら
        if (other.tag == "EnemyBullet")
        {
            //プレイヤーのHPを減少させる
            playerHP -= receveDamageScore;

            //画面を点滅させるコルーチンの開始
            StartCoroutine("damageFlashing");

            //カメラを揺らすコルーチンの開始
            StartCoroutine(shakeCamera(duration, magnitude));

            //ダメージを受けたら対応するHPゲージを黒色にする
            if (playerHP <= 4)
            {
                remainingHp.transform.Find("RemainingHp_5").gameObject.GetComponent<Image>().color = Color.black;
            }
            if (playerHP <= 3)
            {
                remainingHp.transform.Find("RemainingHp_4").gameObject.GetComponent<Image>().color = Color.black;
            }
            if (playerHP <= 2)
            {
                remainingHp.transform.Find("RemainingHp_3").gameObject.GetComponent<Image>().color = Color.black;
            }
            if (playerHP <= 1)
            {
                remainingHp.transform.Find("RemainingHp_2").gameObject.GetComponent<Image>().color = Color.black;
            }
            if (playerHP <= 0)
            {
                remainingHp.transform.Find("RemainingHp_1").gameObject.GetComponent<Image>().color = Color.black;

                //スクリプトをOFFにする
                GetComponent<PlayerMove>().enabled = false;

                //その場で停止させる
                GetComponent<Rigidbody>().velocity = Vector3.zero;

                //死亡状態用に画面を赤くする
                deathStateBoard.color = new Color(1f, 0f, 0f, 0.9f);

                //ゲームオーバーシーンに遷移
                GameObject.FindGameObjectWithTag("SceneManager").GetComponent<GameOverSceneChange>().ToGameOverScene();
            }
        }
    }

    //画面を点滅
    IEnumerator damageFlashing()
    {
        //被ダメージ演出用ボードを薄い赤色にする
        damageBoard.color = new Color(1f, 0f, 0f, 0.7f);

        //次の処理に移行するまでの待機時間
        yield return new WaitForSeconds(0.15f);

        //被ダメージ演出用ボードを透明にする
        damageBoard.color = Color.clear;
    }

    //カメラを揺らす
    IEnumerator shakeCamera(float duration, float magnitude)
    {
        //時間計測用
        var elapsed = 0f;

        //指定の時間を経過するまで
        while (elapsed < duration)
        {
            //カメラのXY(縦横)位置を動かす
            transformCamera.localPosition = new Vector3(pos.x + Random.Range(-1f, 1f) * magnitude, pos.y + Random.Range(-1f, 1f) * magnitude, pos.z);

            //経過時間
            elapsed += Time.deltaTime;

            //次フレームで処理を再開。
            yield return null;
        }
        //揺れる前の位置にカメラを戻す
        transformCamera.localPosition = pos;
    }
}