using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSceneChange : MonoBehaviour
{
    //Textをまとめたオブジェクトを取得
    [SerializeField] GameObject gameOver;

    //Textを配列でまとめて取得
    Transform[] transforms;

    //TextのXY座標取得用
    float x, y;

    //Textの移動速度
    [SerializeField] float moveCharacterSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //親と全ての子オブジェクトのTransformコンポーネントを取得
        transforms = gameOver.GetComponentsInChildren<Transform>();

        StartCoroutine("MoveCharacter");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator MoveCharacter()
    {
        for (int i = 1; i < transforms.Length; i++)
        {
            //現在のX座標を取得
            x = transforms[i].transform.localPosition.x;

            //2で割り切れたら
            if (i % 2 == 0)
            {
                //Y座標が0より小さかったら
                while (transforms[i].localPosition.y < 0)
                {
                    //Y座標をプラス
                    y = transforms[i].transform.localPosition.y + moveCharacterSpeed * Time.deltaTime;

                    //Y座標が0より大きくなった場合は0を代入する
                    transforms[i].transform.localPosition = new Vector3(x, Mathf.Min(y, 0f), 0f);

                    yield return null;
                }
            }
            else
            {
                //Y座標が0より大きかったら
                while (transforms[i].localPosition.y > 0)
                {
                    //Y座標をマイナス
                    y = transforms[i].transform.localPosition.y - moveCharacterSpeed * Time.deltaTime;

                    //Y座標が0より小さくなった場合は0を代入する
                    transforms[i].transform.localPosition = new Vector3(x, Mathf.Max(y, 0f), 0f);

                    yield return null;
                }
            }
        }
    }
}