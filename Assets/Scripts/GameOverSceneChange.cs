using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//シーンを扱う為の名前空間
using UnityEngine.SceneManagement;

public class GameOverSceneChange : MonoBehaviour
{
    [SerializeField] Image fadeBoard;

    //色、透明度
    float red, green, blue, alfa;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToGameOverScene()
    {
        StartCoroutine("fadeOut");
    }

    IEnumerator fadeOut()
    {
        yield return new WaitForSeconds(5f);

        //フェードアウト
        var isFadeOut = true;

        //何秒かけて不透明にするか
        var duration = 5f;

        //colorの個々の値は取得できても変更できない為、変数に代入する
        red = fadeBoard.color.r;

        green = fadeBoard.color.g;

        blue = fadeBoard.color.b;

        alfa = fadeBoard.color.a;

        while (isFadeOut)
        {
            //durationの時間で暗転するよう、アルファ値を加算
            alfa += Time.deltaTime / duration;

            //アルファ値以外は変更せずにcolorに値を代入
            fadeBoard.color = new Color(red, green, blue, alfa);

            //透明度が1(完全に不透明)になったら
            if (alfa >= 1)
            {
                //フェードアウト終了
                isFadeOut = false;
            }

            yield return null;
        }

        yield return new WaitForSeconds(1f);

        //ゲームオーバーシーンに切り替え
        SceneManager.LoadScene("GameOverScene");
    }
}